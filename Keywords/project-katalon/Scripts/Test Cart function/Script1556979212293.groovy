import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://34.238.152.92:8085/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Page_ProjectBackend/a_Carts_have 1 product'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/Cart table'), 0)

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/p_Total price'), 'Total price: 20,000 THB')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Garden_amount'), '2')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/p_Total price'), 'Total price: 40,000 THB')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_confirm'))

WebUI.getAlertText()

WebUI.closeBrowser()

