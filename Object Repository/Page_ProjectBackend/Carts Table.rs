<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Table taht keep all Carts data</description>
   <name>Carts Table</name>
   <tag></tag>
   <elementGuidId>94d43bbe-a9f9-442e-93ee-540794b47679</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;add-row&quot;]/div/table/tbody</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;add-row&quot;]/div/table/tbody</value>
   </webElementProperties>
</WebElementEntity>
