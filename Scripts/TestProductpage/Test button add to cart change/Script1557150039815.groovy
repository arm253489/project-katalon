import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://34.238.152.92:8085/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Garden'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_banana'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Orange'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Papaya'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_Rambutan'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_add to cart_Garden'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_add to cart_Garden'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_add to cart_banana'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_add to cart_banana'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_add to cart_Orange'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_add to cart_Orange'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_add to cart_Papaya'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_add to cart_Papaya'))

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/button_add to cart_Rambutan'), 0)

WebUI.verifyElementNotClickable(findTestObject('Page_ProjectBackend/button_already added_Rambutan'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Garden'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_banana'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Orange'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_papaya'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_already added_Rambutan'), 'already added')

WebUI.closeBrowser()

