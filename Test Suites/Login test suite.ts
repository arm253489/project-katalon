<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Testsuit for all user login</description>
   <name>Login test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e682aa62-592b-45af-854f-bb0e39ce848e</testSuiteGuid>
   <testCaseLink>
      <guid>7052dd94-8adb-4cfd-bfbe-9657886f2513</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Log inTest/Test login by varible</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>33f9369f-4cb6-428b-bc44-0660547e8d50</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/userdata</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>33f9369f-4cb6-428b-bc44-0660547e8d50</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c9744428-4285-4000-9253-b9e06b8f8909</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>33f9369f-4cb6-428b-bc44-0660547e8d50</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>8fc503a7-ff11-45a8-8741-2d69671f9150</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>33f9369f-4cb6-428b-bc44-0660547e8d50</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>menu1</value>
         <variableId>1628c84b-a873-4832-a260-95030a407fbb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>33f9369f-4cb6-428b-bc44-0660547e8d50</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>menu2</value>
         <variableId>6781ca83-1f48-4804-8a68-b773337996f4</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
